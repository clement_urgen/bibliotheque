import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import router from "./Routes";
import jwt from "./Configs/jwt";
import swaggerUi from 'swagger-ui-express';
import yamljs from 'yamljs';
const swaggerDoc = yamljs.load('swagger.yaml');

export default class Server{

    /**
     * Config server
     * @returns {Express}
     */
    static config(){
        //Token
        const app = express();
        app.use(jwt());

        app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc));
        app.use('/uploads', express.static('uploads'));

        //Configuration de l'app
        app.use(bodyParser.urlencoded({extended: false}));
        app.use(bodyParser.json());
        app.use(cors({origin: true}));
        //Configuration des routes de l'API depuis routes.js
        app.use('/', router);
        return app;
    }
}
