import { Router } from 'express';
import UserController from "./Controllers/UserController";
import GenreController from "./Controllers/GenreController";
import CategorieController from "./Controllers/CategorieController";
import AuteurController from "./Controllers/AuteurController";
import EditionController from "./Controllers/EditionController";
import LivreController from "./Controllers/LivreController";
import Multer from "./Utils/Multer";
import Auth from "./Utils/Auth";

const router = Router();

/** 
*Users
*/
router.get('/users', UserController.list);
router.get('/users/:id', UserController.details);
router.post('/users/auth', UserController.auth);
router.post('/users', Auth.isAllowed([10]),  UserController.store);
router.put('/users/:id', Auth.isAllowed([10]), UserController.update);
router.delete('/users/:id', Auth.isAllowed([10]), UserController.remove);

/**
 * Genres
 */
router.get('/genres', GenreController.list);
router.post('/genres', Auth.isAllowed([10]), GenreController.store);
router.put('/genres/:id', Auth.isAllowed([10]), GenreController.update);
router.delete('/genres/:id', Auth.isAllowed([10]), GenreController.remove);

/**
 * Categorie
 */
router.get('/categories', CategorieController.list);
router.post('/categories', Auth.isAllowed([10]), CategorieController.store);
router.put('/categories/:id', Auth.isAllowed([10]), CategorieController.update);
router.delete('/categories/:id', Auth.isAllowed([10]), CategorieController.remove);

/**
 * Auteur
 */
router.get('/auteurs', AuteurController.list);
router.get('/auteurs/:id', AuteurController.details);
router.post('/auteurs', Auth.isAllowed([10]),Multer.upload('auteurs', 'image'), AuteurController.store);
router.put('/auteurs/:id', Auth.isAllowed([10]), AuteurController.update);
router.delete('/auteurs/:id', Auth.isAllowed([10]), AuteurController.remove);
router.put('/auteurs/:id/image', Auth.isAllowed([10]), Multer.upload('auteurs', 'image'), AuteurController.updateImage);


/**
 * Edition
 */
router.get('/editions', EditionController.list);
router.post('/editions', Auth.isAllowed([10]),Multer.upload('editions','logo'),EditionController.store);
router.put('/editions/:id', Auth.isAllowed([10]), EditionController.update);
router.delete('/editions/:id', Auth.isAllowed([10]), EditionController.remove);
router.put('/editions/:id/logo', Auth.isAllowed([10]), Multer.upload('editions', 'logo'), EditionController.updateLogo);

/**
 * Livre
 */
router.get('/livres/:id', LivreController.details);
router.post('/livres', Auth.isAllowed([10]), Multer.upload('livres', 'couverture'), LivreController.store);
router.get('/livres', LivreController.list);
router.post('/livres', Auth.isAllowed([10]), LivreController.store);
router.put('/livres/update/:id', LivreController.update);
router.delete('/livres/:id', Auth.isAllowed([10]), LivreController.remove);
router.put('/livres/:id/couverture', Auth.isAllowed([10]), Multer.upload('livres', 'couverture'), LivreController.updateCouverture);

export default router;