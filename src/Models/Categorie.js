import { Schema, model } from 'mongoose';

const CategorieSchema = new Schema({
    libelle: {type: String, required: true}
})

export default model('Categorie', CategorieSchema);
