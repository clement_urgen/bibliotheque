import { Schema, model } from 'mongoose';

const GenreSchema = new Schema({
    libelle: {type: String, required: true}
});

export default model('Genre', GenreSchema);
