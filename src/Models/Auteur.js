import { Schema, model} from 'mongoose';

const AuteurSchema = new Schema({

	nom: {type: String, required: true},
	prenom: {type: String, required: false},
	naissance: {type: Date, required: true},
	description: {type: String, required: true},
	mort: {type: String, required: false},
	image: {type: String, required: false}
});

export default model('Auteur', AuteurSchema);