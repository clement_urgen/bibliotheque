import { Schema, model } from 'mongoose';

const EditionSchema = new Schema({
    nom: {type: String, required: true},
    logo: {type: String, required: false}
});

export default model('Edition', EditionSchema);
