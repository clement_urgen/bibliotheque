import {Schema ,model} from 'mongoose';

const LivreSchema = new Schema({
	titre: {type: String, required: true},
	parution: {type: Date, required: true},
	couverture: {type: String, required: true},
	lien: {type: String, required: true},
	synopsis: {type: String, required: false},
	auteur: [{
		type: Schema.Types.ObjectId, ref: 'Auteur'
	}],
	edition: [{
		type: Schema.Types.ObjectId, ref: 'Edition'
	}],
	categorie: [{
		type: Schema.Types.ObjectId, ref: 'Categorie'
	}],
	genres: [{
			type: Schema.Types.ObjectId, ref: 'Genre'
	}],
	created_at: {type: Date, default: Date.now()},
});
export default model('Livre', LivreSchema);
