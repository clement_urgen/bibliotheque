export default class Utils{
	
    /**
     * Generate string random
     * @returns {string}
     */
    static generateStringRandom(){
        return Math.random().toString(36).substr(2) + Math.random().toString(36).substr(2);
    }
}
