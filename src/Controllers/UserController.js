import User from "../Models/User";
import jsonwebtoken from 'jsonwebtoken';

export default class UserController{

    static async auth(req, res){
        let status = 200;
        let body = {};

        try{
            let {email, password} = req.body;
            let user = await User.findOne({'email': email}).select('-__v');
            if(user && password === user.password){
                //L'utilisateur existe dans la base de données
                let {JWT_SECRET} = process.env;

                let token = jsonwebtoken.sign({sub: user._id}, JWT_SECRET);
                body = {user, token};
            }else{
                status = 401;
                new Error('Unauthorized');
            }
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User authentication',
                message: e.message || 'An error is occured into user auth',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Get list of users
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            //Récupération utilisateurs
            let users = await User.find().select('-__v -password');
            body = {users};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User list',
                message: e.message || 'An error is occured into user list',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Get details of user
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async details(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let user = await User.findById(id).select('-__v -password');
            body = {user};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User details',
                message: e.message || 'An error is occured into user details',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Create one user
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async store(req, res){
        let status = 200;
        let body = {};

        try{
            let user = await User.create(req.body);
            body={user};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User create',
                message: e.message || 'An error is occured into user create',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Update one user
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let user = await User.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v -password');
            body = {user};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User update',
                message: e.message || 'An error is occured into user update',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Remove one user
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            await User.findByIdAndDelete(id);
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'User remove',
                message: e.message || 'An error is occured into user remove',
            }
        }
        return res.status(status).json(body);
    }
}
