import Edition from "../Models/Edition";
import fs from "fs";

export default class EditionController{

    /**
     * Get list of editions
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let editions = await Edition.find().select('-__v');
            body = {editions};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Edition list',
                message: e.message || 'An error is occured into edition list',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Create one edition
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async store(req, res){
        let status = 200;
        let body = {};

        try{
            let edition = await Edition.create(req.body);
            body = {edition};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Edition store',
                message: e.message || 'An error is occured into edition store',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Update one edition
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let edition = await Edition.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v');
            body = {edition};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Edition update',
                message: e.message || 'An error is occured into edition update',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Update one edition logo
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async updateLogo(req, res){
        let status = 200 ;
        let body = {};

        try{
            let edition = await Edition.findById(req.params.id)
            if(fs.existsSync(`./${edition.logo}`)){
                await  fs.unlinkSync(`./${edition.logo}`);
            }
            edition.logo = req.body.logo;
            await edition.save();
            body = {edition};
        }catch (e){
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Edition update logo',
                message: e.message || 'An error is occured into edition update logo',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Remove one edition
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let edition = await Edition.findByIdAndDelete(req.params.id);
            if(fs.existsSync(`./${edition.image}`)){
                await fs.unlinkSync(`./${edition.image}`);
            }
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Edition remove',
                message: e.message || 'An error is occured into edition remove',
            }
        }
        return res.status(status).json(body);
    }
}
