import Categorie from "../Models/Categorie";

export default class CategorieController{

    /**
     * Get list of categories
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let categories = await Categorie.find().select('-__v');
            body = {categories};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Categorie list',
                message: e.message || 'An error is occured into categorie list',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Create one categorie
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async store(req, res){
        let status = 200;
        let body = {};

        try{
            let categorie = await Categorie.create(req.body);
            body = {categorie};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Categorie store',
                message: e.message || 'An error is occured into categorie store',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Update one categorie
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let categorie = await Categorie.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v');
            body = {categorie};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Categorie update',
                message: e.message || 'An error is occured into categorie update',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Remove one categorie
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            await Categorie.findByIdAndDelete(id);
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Categorie remove',
                message: e.message || 'An error is occured into categorie remove',
            }
        }
        return res.status(status).json(body);
    }
}
