import Genre from "../Models/Genre";

export default class GenreController{

    /**
     * Get list of genres
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async list(req, res){
        let status = 200;
        let body = {};

        try{
            let genres = await Genre.find().select('-__v');
            body = {genres};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Genre list',
                message: e.message || 'An error is occured into genre list',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Create one genre
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async store(req, res){
        let status = 200;
        let body = {};

        try{
            let genre = await Genre.create(req.body);
            body = {genre};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Genre store',
                message: e.message || 'An error is occured into genre store',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Update one genre
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async update(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            let genre = await Genre.findByIdAndUpdate(id, req.body, {new: true})
                .select('-__v');
            body = {genre};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Genre update',
                message: e.message || 'An error is occured into genre update',
            }
        }
        return res.status(status).json(body);
    }

    /**
     * Remove one genre
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async remove(req, res){
        let status = 200;
        let body = {};

        try{
            let {id} = req.params;
            await Genre.findByIdAndDelete(id);
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Genre remove',
                message: e.message || 'An error is occured into genre remove',
            }
        }
        return res.status(status).json(body);
    }
}
