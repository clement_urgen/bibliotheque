import Livre from "../Models/Livre";
import fs from "fs";

export default class LivreController{

	/**
	 * Get details of livre
	 * @param req
	 * @param res
	 * @returns {Promise<any>}
	 */
	static async details(req, res){
		let status = 200;
		let body = {};

		try{
			let {id} = req.params.id;
			let livre = await Livre.findById(id).populate('genres').populate('edition').populate('auteur').populate('categorie').select('__v');
			body = {livre};
		}catch (e) {
			status = status !== 200 ? status : 500;
			body = {
				error: e.error || 'livre details',
				message: e.message || 'An error is occured into livre details',
			}
		}
		return res.status(status).json(body);
	}

	/**
	 * Get list of livre
	 * @param req
	 * @param res
	 * @returns {Promise<any>}
	 */
	static async list(req, res){
		let status = 200;
		let body = {};

		try{
			let livres = await Livre.find().populate('genres').populate('edition').populate('auteur').populate('categorie').select('-__v');
			body = {livres};
		}catch (e) {
			status = status !== 200 ? status : 500;
			body = {
				error: e.error || 'livre list',
				message: e.message || 'An error is occured into livre list',
			}
		}
		return res.status(status).json(body);
	}

	/**
	 * Create one livre
	 * @param req
	 * @param res
	 * @returns {Promise<any>}
	 */
	static async store(req, res){
		let status = 200;
		let body = {};

		try{
			let livre = await Livre.create(req.body);
			body = {livre};
		}catch (e) {
			status = status !== 200 ? status : 500;
			body = {
				error: e.error || 'livre create',
				message: e.message || 'An error is occured into livre create',
			}
		}
		return res.status(status).json(body);
	}

	/**
	 * Update one livre
	 * @param req
	 * @param res
	 * @returns {Promise<any>}
	 */
	static async update(req, res){
		let status = 200;
		let body = {};

		try{
			let {id} = req.params;
			let livre = await Livre.findByIdAndUpdate(id, req.body, {new: true})
				.select('-__v');
			body = {livre};
		}catch (e) {
			status = status !== 200 ? status : 500;
			body = {
				error: e.error || 'livre update',
				message: e.message || 'An error is occured into livre update',
			}
		}
		return res.status(status).json(body);
	}

	/**
	 * Update one livre couverture
	 * @param req
	 * @param res
	 * @returns {Promise<any>}
	 */
	static async updateCouverture(req, res){
		let status = 200 ;
		let body = {};

		try{
			let livre = await Livre.findById(req.params.id)
			if(fs.existsSync(`./${livre.couverture}`)){
				await  fs.unlinkSync(`./${livre.couverture}`);
			}
			livre.couverture = req.body.couverture;
			await livre.save();
			body = {livre};
		}catch (e){
			status = status !== 200 ? status : 500;
			body = {
				error: e.error || 'Livre couverture update ',
				message: e.message || 'An error is occured into livre couverture update ',
			}
		}
		return res.status(status).json(body);
	}

	/**
	 * Remove one livre
	 * @param req
	 * @param res
	 * @returns {Promise<any>}
	 */
	static async remove(req, res){
		let status = 200;
		let body = {};

		try{
			await Livre.findByIdAndDelete(req.params.id);
			if(fs.existsSync(`./${livre.couverture}`)){
				await  fs.unlinkSync(`./${livre.couverture}`);
			}
		}catch (e) {
			status = status !== 200 ? status : 500;
			body = {
				error: e.error || 'livre delete',
				message: e.message || 'An error is occured into livre delete',
			}
		}
		return res.status(status).json(body);
	}

}