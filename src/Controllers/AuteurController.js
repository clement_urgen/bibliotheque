import Auteur from "../Models/Auteur";
import fs from "fs";

export default class AuteurController{

	/**
	 * Get details of auteur
	 * @param req
	 * @param res
	 * @returns {Promise<any>}
	 */
	static async details(req, res){
		let status = 200;
		let body = {};

		try{
			let {id} = req.params.id;
			let auteur = await Auteur.findById(id).select('__v');
			body = {auteur};
		}catch (e) {
			status = status !== 200 ? status : 500;
			body = {
				error: e.error || 'Auteur details',
				message: e.message || 'An error is occured into auteur details',
			}
		}
		return res.status(status).json(body);
	}

	/**
	 * Get list of auteur
	 * @param req
	 * @param res
	 * @returns {Promise<any>}
	 */
	static async list(req, res){
		let status = 200;
		let body = {};

		try{
			let auteurs = await Auteur.find().select('-__v');
			body = {auteurs};
		}catch (e) {
			status = status !== 200 ? status : 500;
			body = {
				error: e.error || 'Auteur list',
				message: e.message || 'An error is occured into auteur list',
			}
		}
		return res.status(status).json(body);
	}

	/**
	 * Create one auteur
	 * @param req
	 * @param res
	 * @returns {Promise<any>}
	 */
	static async store(req, res){
		let status = 200;
		let body = {};

		try{
			let auteur = await Auteur.create(req.body);
			body = {auteur};
		}catch (e) {
			status = status !== 200 ? status : 500;
			body = {
				error: e.error || 'Auteur create',
				message: e.message || 'An error is occured into auteur create',
			}
		}
		return res.status(status).json(body);
	}

	/**
	 * Update one auteur
	 * @param req
	 * @param res
	 * @returns {Promise<any>}
	 */
	static async update(req, res){
		let status = 200;
		let body = {};

		try{
			let {id} = req.params;
			let auteur = await Auteur.findByIdAndUpdate(id, req.body, {new: true})
				.select('-__v');
			body = {auteur};
		}catch (e) {
			status = status !== 200 ? status : 500;
			body = {
				error: e.error || 'Auteur update',
				message: e.message || 'An error is occured into auteur update',
			}
		}
		return res.status(status).json(body);
	}

	/**
	 * Remove one auteur
	 * @param req
	 * @param res
	 * @returns {Promise<any>}
	 */
	static async remove(req, res){
		let status = 200;
		let body = {};

		try{
            let auteur = await Auteur.findByIdAndDelete(req.params.id);
            if(fs.existsSync(`./${auteur.image}`)){
                await fs.unlinkSync(`./${auteur.image}`);
            }
        }catch (e) {
			status = status !== 200 ? status : 500;
			body = {
				error: e.error || 'Auteur delete',
				message: e.message || 'An error is occured into auteur delete',
			}
		}
		return res.status(status).json(body);
	}

	 /**
     * Update thumnail of one auteur
     * @param req
     * @param res
     * @returns {Promise<any>}
     */
    static async updateImage(req, res){
        let status = 200;
        let body = {};

        try{
            let auteur = await Auteur.findById(req.params.id);
            if(fs.existsSync(`./${auteur.Image}`)){
                await fs.unlinkSync(`./${auteur.image}`);
            }
            auteur.image = req.body.image;
            await auteur.save();
            body = {auteur};
        }catch (e) {
            status = status !== 200 ? status : 500;
            body = {
                error: e.error || 'Update auteur image',
                message: e.message || 'An error is occured into auteur image update',
            }
        }
        return res.status(status).json(body);
    }
}