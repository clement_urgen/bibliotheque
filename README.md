## A propos
L'API pour le site Bibliotèque

## Base de Données
A recupérer en reponse de mail par Clément.

Importer les collections dans MongoBD.

## Installation
installation des libraries du package.json
npm install

lancer l'API
npm run dev

## les tâches effectuées
 Clément Urgen:
  - Création des Models User, Categorie, Genre
  - Controller User, Categorie, Genre
  - Création Database ,Server 
  - Ajout des routes

 Prinda Sivakumaran:
  - Swagger
  - Création des Models Auteur, Livre, Edition
  - Controller Auteur, Livre, Edition
  - Ajout de l'authentification
  - Ajout des routes

